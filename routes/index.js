var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});

var ctrlProfile = require('../controllers/profile');
var ctrlAuth = require('../controllers/authentication');

// profile
router.get('/profile', auth, ctrlProfile.profileRead);

// authentication
router.post('/register', auth, ctrlAuth.register);
router.post('/login', ctrlAuth.login);
router.get('/users', auth, ctrlAuth.users);
router.get('/users/softdelete/:id', auth, ctrlAuth.userSoftDelete);

router.get('/users/:id', auth, ctrlAuth.retrieveUsers);
router.put('/users/:id', auth, ctrlAuth.update);


router.post('/attendance', auth, ctrlAuth.addAttendance);
router.get('/attendance', auth, ctrlAuth.attendanceList);

router.get('/tubes', auth, ctrlAuth.listTube);
router.put('/tubes/:id', auth, ctrlAuth.updateTube);
router.post('/tube/add', auth, ctrlAuth.addTube);


module.exports = router;