var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Tubes = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    cost: {
        type: Number,
        required: true
    }
}, { timestamps: { createdAt: 'created_at' } });

module.exports = mongoose.model('Tubes', Tubes);