var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Attendance = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    check_in: {
        type: Date,
        default: Date.now,
    },
    tubes: [{
        type: Schema.Types.ObjectId,
        ref: 'BookedTubes'
    }],
    total_cost : {
        type : Number,
        required : true
    }
}, { timestamps: { createdAt: 'checkin_at' } });

module.exports = mongoose.model('Attendance', Attendance);