var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new mongoose.Schema({
    mobile: {
        type: Number,
        unique: true,
        required: true
    },
    amount: {
        type: Number,
        default: 0
    },
    role: {
        type: Number,
        required: true
    },
    bookings: [{
        type: Schema.Types.ObjectId,
        ref: 'Attendance'
    }],
    is_active: {
        type: Boolean,
        default: true
    },
    name: {
        type: String,
        required: true
    },
    hash: String,
    salt: String
}, { timestamps: { createdAt: 'created_at' } });

userSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
}

userSchema.methods.validPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

userSchema.methods.generateJwt = function () {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        name: this.name,
        exp: parseInt(expiry.getTime() / 1000),
    }, "MY_SECRET"); // DO NOT KEEP YOUR SECRET IN THE CODE!
};

module.exports = mongoose.model('User', userSchema);