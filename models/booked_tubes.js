var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BookedTubes = new mongoose.Schema({
    tube: {
        type: Schema.Types.ObjectId,
        ref: 'Tubes'
    },
    quantity : {
        type : Number,
        required : true
    },
    cost: {
        type: Number,
        default: 180
    }
}, { timestamps: { createdAt: 'created_at' } });

module.exports = mongoose.model('BookedTubes', BookedTubes);