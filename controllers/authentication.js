var passport = require('passport');
var user = require('./../models/users');
var tubes = require('./../models/tubes');
var booked_tubes = require('./../models/booked_tubes');
var Attendance = require('./../models/attendance');
var jwt = require('express-jwt');
var logger = require('morgan');
module.exports.register = function (req, res) {
    var userModel = new user({
        name: req.body.name,
        mobile: req.body.mobile,
        is_active: true,
        role: req.body.role
    });

    userModel.setPassword(req.body.password);

    userModel.save(function (err, user) {
        var token;
        if (err) {
            res.status(404);
            var msg = '';
            if (err.code == 11000) {
                msg = err;//'Username is already exists..!'
            } else {
                msg = err;
            }
            res.json({
                "err": msg
            });
        } else {
            token = user.generateJwt();
            res.status(200);
            var result = {
                name: user.name,
                id: user._id,
                role: user.role,
                mobile: user.mobile,
                token: token
            }
            res.json(result);
        }
    });
};

module.exports.login = function (req, res) {
    passport.authenticate('local', function (err, user, info) {
        var token;

        if (err) {
            res.status(404).json(err);
            return;
        }

        if (user) {
            token = user.generateJwt();
            res.status(200);
            res.json({
                id: user._id,
                name: user.name,
                mobile: user.mobile,
                role: user.role,
                token: token
            });
        } else {
            res.status(401).json(info);
        }
    })(req, res);
};

module.exports.update = function (req, res) {
    user.findById(req.params.id, function (err, userModel) {
        userModel.setPassword(req.body.password);
        userModel.name = req.body.name;
        if (err) return handleError(err);
        userModel.save();
        res.send(userModel);
    });
};

module.exports.userSoftDelete = function (req, res) {
    user.findById(req.params.id, function (err, userModel) {
        if (userModel) {
            userModel.is_active = !userModel.is_active;
            if (err) return handleError(err);
            userModel.save((err) => {
                res.json(userModel);
            });
        } else {
            res.status(404).json({ 'error': "User does not found." });
        }
    });
};


module.exports.users = function (req, res) {
    var search = { role: 2 };
    if (req.query) {
        if (req.query.is_active != undefined) {
            search.is_active = req.query.is_active == "true" ? true : false;
        }
    }

    user.find(search)
        .select('name mobile role is_active bookings')
        .exec(function (err, users) {
            if (err) {
                res.json({
                    status: 500,
                    error: err
                });
            }
            res.json(users);
        });
};

module.exports.attendanceList = function (req, res) {
    Attendance.find()
        .select('-__v -updatedAt')
        .populate([{
            path: 'user',
            select: 'name mobile role'
        }, {
            path: 'tubes'
        }])
        .exec(function (err, attendance) {
            if (err) {
                res.json({
                    status: 500,
                    error: err
                });
            }
            res.json(attendance);
        });
};

module.exports.addAttendance = function (req, res) {
    booked_tubes.insertMany(req.body.tubes).then(tube => {
        // res.json(tube);
        var tubes = [];
        var total_cost = 0;
        tube.forEach(value => {
            tubes.push(value);
            total_cost += value.cost;
        })
        var attendance = new Attendance({
            user: req.body.user,
            tubes: tubes,
            total_cost: total_cost
        })
        attendance.save(function (err, attend) {
            if (err) {
                res.json({
                    status: 500,
                    error: err
                });
            }
            user.findById(req.body.user, (err, result) => {
                result.bookings.push(attend._id);
                result.save();
            })
            res.json(attend);
        });
    }).catch(err => {
        res.json(err);
    })

};

module.exports.retrieveUsers = function (req, res) {
    user.findById(req.params.id, function (err, user) {
        if (err) {
            res.json({
                status: 500,
                error: err
            });
        }
        res.json(user);
    });
};

module.exports.addTube = function (req, res) {
    tubes.create(req.body).then((tube) => {
        res.json(tube);
    }).catch(err => {
        res.status(404).json(err);
    })
};

module.exports.updateTube = function (req, res) {
    tubes.findByIdAndUpdate(req.params.id, req.body).then((tube) => {
        res.json({ "message": "updated Successfully" });
    }).catch(err => {
        res.status(404).json(err);
    })
};

module.exports.listTube = function (req, res) {
    tubes.find().then((tube) => {
        res.json(tube);
    }).catch(err => {
        res.status(404).json(err);
    })
};